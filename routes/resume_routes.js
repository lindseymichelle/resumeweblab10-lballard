var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');


// View All resume
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result':result });
        }
    });

});

// View the resume for the given id
router.get('/', function(req, res){
    if(req.query.resume_id === null) {
        res.send('resume_id is null');
    } else {
        resume_dal.getById(req.query.resume_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('resume/resumeViewById', {'result': result});
           }
        });
    }
});

// Return the add a new resume form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    resume_dal.getAllResumeInfo(req.query, function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            console.log(result);
            res.render('resume/resumeAdd', {
                'account_info': result[0][0],
                'companies': result[1],
                'schools': result[2],
                'skills': result[3]
            });
        }
    });
});



// Return the add a new resume form
router.get('/add/selectuser', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    resume_dal.getAllUsers(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeSelectUser', {'result': result});
        }
    });
});

// TODO: error with account_id insert- refer to resume_dal .insert
// View the resume for the given id
router.post('/insert', function(req, res){
    // simple validation
    if(req.query.resume_name === null) {
        res.send('Resume Name must be provided.');
    } else {
        console.log(req.query.account_id);
        console.log(req.query.resume_name);
        // passing all the query parameters (req.query) to the insert function instead of each individually
        resume_dal.insert(req.body.account_id, req.body.resume_name, function(err,result) {
            var resume_id = result.insertId;
            if (err) {
                console.log(err);
                res.send(err);
            } else {
                if(req.body.resume_skill === null) {
                    res.send('A skill must be provided.');
                } else {
                    // passing all the query parameters (req.query) to the insert function instead of each individually
                    resume_dal.save_skills(result.insertId, req.body.skill_id, function(err,skills) {

                        if (err) {
                            console.log(err);
                            res.send(err);
                        } else {
                            if(req.body.resume_company === null) {
                                res.send('A company must be provided.');
                            } else {
                                // passing all the query parameters (req.query) to the insert function instead of each individually
                                resume_dal.save_companies(result.insertId, req.body.company_id, function(err,companies) {
                                    if (err) {
                                        console.log(err);
                                        res.send(err);
                                    } else {
                                        if(req.body.resume_name === null) {
                                            res.send('A school must be provided.');
                                        } else {
                                            // passing all the query parameters (req.query) to the insert function instead of each individually
                                            resume_dal.save_schools(result.insertId, req.body.school_id, function(err,schools) {
                                                if (err) {
                                                    console.log(err);
                                                    res.send(err);
                                                } else {
                                                    resume_dal.edit(resume_id, function(err,result) {
                                                        res.render('resume/resumeUpdate', {resume:result[0][0], skills:result[3],
                                                            companies: result[1], schools: result[2]});
                                                    });
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }

                    });
                }
            }
        });
    }
});




// Delete an resume for the given resume_id
router.get('/delete', function(req, res){
    if(req.query.resume_id === null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.delete(req.query.resume_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/resume/all');
            }
        });
    }
});


router.post('/update', function(req, res) {
    resume_dal.update(req.body, function(err, result){
        res.send('Success!');
    });
});



router.get('/edit', function(req, res){
    if(req.query.resume_id === null) {
        res.send('A resume id is required');
    }
    else {
        resume_dal.edit(req.query.resume_id, function(err, result){
            res.render('resume/resumeUpdate', {resume: result[0][0], skills:result[3],
                companies: result[1], schools: result[2]});
        });
    }

});


module.exports = router;
