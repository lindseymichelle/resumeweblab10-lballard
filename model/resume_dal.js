/**
 * Created by lindseyballard on 4/12/17.
 */

var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view account_view as
 select a.* from account a;
 */


exports.getAll = function(callback) {
    var query = 'SELECT r.*, a.account_id, a.first_name, a.last_name ' +
        'FROM resume r LEFT JOIN account a on r.account_id = a.account_id ' +
        'ORDER BY a.first_name';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};



exports.getAllResumeInfo = function(params, callback) {
    var query = 'CALL account_resume_getinfo(?)'; // where do I pass this data to the resume_routes and HOW
    var queryData = [params.account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


exports.getAllUsers = function(callback) {
    var query = 'SELECT * FROM account' ;

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(account_id, callback) {
    var query = 'SELECT r.*, a.first_name, a.last_name ' +
        'FROM resume r LEFT JOIN account a on r.account_id = a.account_id ' +
        'WHERE r.resume_id = ?;';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(account_id, resume_name, callback) {

    var query = 'INSERT INTO resume (account_id, resume_name) VALUES (?,?)';
    // var queryData = [account_id, resume_name];

    connection.query(query, [account_id, resume_name], function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE resume SET resume_name = (?,?) ';
    var queryData = [params.resume_name, params.account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err,result);
    });
};


exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume WHERE resume_id = (?)';
    var queryData = [resume_id];

    connection.query(query, resume_id, function(err, result) {
        callback(err, result);
    });

};


exports.save_schools = function (resume_id, schoolIdArray, callback) {
    var query = 'INSERT INTO  resume_school (resume_id, school_id) VALUES ?';


    var resumeSchoolData = [];
    if (schoolIdArray.constructor === Array) {
        for (var i = 0; i < schoolIdArray.length; i++) {
            resumeSchoolData.push([resume_id, schoolIdArray[i]]);
        }
    }
    else {
        resumeSchoolData.push([resume_id, schoolIdArray]);
    }

    connection.query(query, [resumeSchoolData], function(err, result) {
        callback(err, result);
    });
};


exports.save_skills = function (resume_id, skillIdArray, callback) {
    var query = 'INSERT INTO  resume_skill (resume_id, skill_id) VALUES ?';
    var resumeSkillData = [];
    if (skillIdArray.constructor === Array) {
        for (var i = 0; i < skillIdArray.length; i++) {
            resumeSkillData.push([resume_id, skillIdArray[i]]);
        }
    }
    else {
        resumeSkillData.push([resume_id, skillIdArray]);
    }

    connection.query(query, [resumeSkillData], function(err, result) {
        callback(err, result);
    });
};



exports.save_companies = function (resume_id, companyIdArray, callback) {
    var query = 'INSERT INTO  resume_company (resume_id, company_id) VALUES ?';
    var resumeCompanyData = [];
    if (companyIdArray.constructor === Array) {
        for (var i = 0; i < companyIdArray.length; i++) {
            resumeCompanyData.push([resume_id, companyIdArray[i]]);
        }
    }
    else {
        resumeCompanyData.push([resume_id, companyIdArray]);
    }

    connection.query(query, [resumeCompanyData], function(err, result) {
        callback(err, result);
    });

};



/*
 DROP PROCEDURE IF EXISTS account_resume_getinfo;

 DELIMITER //
 CREATE PROCEDURE account_resume_getinfo (_account_id int)
 BEGIN

 SELECT * FROM account WHERE account_id = _account_id;

 SELECT c.*, ac.* FROM company c
 LEFT JOIN account_company ac ON c.company_id = ac.company_id AND account_id = _account_id;

 SELECT s.*, sa.* FROM school s
 LEFT JOIN account_school sa ON s. school_id = sa.school_id AND account_id = _account_id;

 SELECT sk.*, ska.* FROM skill sk
 LEFT JOIN account_skill ska ON sk.skill_id = ska.skill_id AND account_id = _account_id;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL account_getinfo (2);
 */

/*  Stored procedure used in this example
 DROP PROCEDURE IF EXISTS account_getinfo;

 DELIMITER //
 CREATE PROCEDURE account_getinfo(_account_id int)
 BEGIN
 SELECT * FROM account WHERE account_id = _account_id;

 END; //

 # Call the Stored Procedure
 CALL account_getinfo (4);



 DROP PROCEDURE IF EXISTS resume_account_getinfo;

 DELIMITER //
 CREATE PROCEDURE resume_account_getinfo (_resume_id int)
 BEGIN

 SELECT * FROM resume WHERE resume_id = _resume_id;

 SELECT c.*, rc.company_id FROM company c
 LEFT JOIN resume_company rc ON c.company_id = rc.company_id AND resume_id = _resume_id;

 SELECT s.*, rs.school_id FROM school s
 LEFT JOIN resume_school rs ON s.school_id = rs.school_id AND resume_id = _resume_id;

 SELECT sk.*, rsk.skill_id FROM skill sk
 LEFT JOIN resume_skill rsk ON sk.skill_id = rsk.skill_id AND resume_id = _resume_id;

 END //
 DELIMITER ;

 */

exports.edit = function(resume_id, callback) {
    var query = 'CALL resume_account_getinfo(?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


